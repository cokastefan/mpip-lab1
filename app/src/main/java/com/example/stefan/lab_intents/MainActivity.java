package com.example.stefan.lab_intents;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private String TAG;
    private Button mExplicitStartButton;
    private Button mImplicitStartButton;
    private Button mShareButton;
    private Button mViewPhotoButton;
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: started main activity");
        TAG = this.getLocalClassName();

        mExplicitStartButton = (Button) findViewById(R.id.explicitButton);
        mImplicitStartButton = (Button) findViewById(R.id.implicitButton);
        mShareButton = (Button) findViewById(R.id.shareButton);
        mViewPhotoButton = (Button) findViewById(R.id.photoButton);

        mTextView = (TextView) findViewById(R.id.textView);


        mExplicitStartButton.setOnClickListener(
                view -> {
                    Log.d(TAG, "onClick: starting explicit activity");

                    startActivityForResult(new Intent(MainActivity.this, ExplicitActivity.class), 1);
                }
        );

        mImplicitStartButton.setOnClickListener(
                view -> {
                    Log.d(TAG, "onClick: starting implicit activity");

                    Intent intent = new Intent();
                    intent.setAction("com.example.stefan.lab_intents.IMPLICIT_ACTION");
                    intent.setType("text/plain");
                    startActivityForResult(intent, 1);
                }
        );

        mShareButton.setOnClickListener(
                view -> {
                    Log.d(TAG, "onClick: sending message");
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TITLE, "MPiP Send Title");
                    intent.putExtra(Intent.EXTRA_TEXT, "Content sent from MainActivity ");
                    startActivity(intent);
                }
        );

        mViewPhotoButton.setOnClickListener(
                view -> {
                    Log.d(TAG, "onClick: opening image");
                    Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    getIntent.setType("image/*");

                    Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    pickIntent.setType("image/*");

                    Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

                    startActivityForResult(chooserIntent, 2);
                }
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                mTextView.setText(data.getStringExtra("editTextValue"));
                Log.e(TAG, "onActivityResult: " + data.getStringExtra("editTextValue"));
            } else if (resultCode == RESULT_CANCELED) {
                mTextView.setText("User canceled");
            }
        }

        if (requestCode == 2) {
        }
    }
}
