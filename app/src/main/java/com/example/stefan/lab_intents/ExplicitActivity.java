package com.example.stefan.lab_intents;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ExplicitActivity extends AppCompatActivity {

    private String TAG;
    private EditText mEditText;
    private Button mCancel;
    private Button mOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explicit);
        TAG = this.getLocalClassName();

        Log.d(TAG, "onCreate: started explicit activity");

        mEditText = (EditText) findViewById(R.id.inputText);

        mOk = (Button) findViewById(R.id.okayButton);
        mCancel = (Button) findViewById(R.id.cancelButton);

        mOk.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.putExtra("editTextValue", mEditText.getText().toString());
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
        );

        mCancel.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                }
        );
    }

}
